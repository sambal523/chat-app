//node defined importations
import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import Dialog from 'material-ui/Dialog';
import 'simplebar'; // or e"import SimpleBar from 'simplebar';" if you want to use it manually.
import 'simplebar/dist/simplebar.css';

//stylings importations
import '../Stylings/chatRoom.css';
import ArrangeComponents from '../Utility/chatRoom.js';


//icons
import RightChevron from 'material-ui/svg-icons/navigation/chevron-right';
import LeftChevron from 'material-ui/svg-icons/navigation/chevron-left';
import ListIcon from 'material-ui/svg-icons/action/list';
import emojis from '../Data/emojis.json';


//Load the full build.
var _loadash = require('lodash');


//definitions
var $ = require("jquery");

//definition of the main component starts here
class TalkHomePage extends React.Component {
    constructor () {
        super();

        //fields definition
        this.state = {
            codeName : "",
            roomName : "",
            roomPassword : "",
            color : "",
            whoIsTyping : '',
            imageUrl: "",
            sideBarClosed : true,
            menuBarClosed : true,
            emojiBarClosed : true,
            openImageDialog : false,
            allMessagesTooltip : [],
            allOthersIcon : [],
            socket: null,
            allEmojiIcons : []
        }
    };


    //
    componentDidMount = () => {
        //connect to server
        this.setState({socket: this.props.socket});
        this.setState({codeName: this.props.adminMessage.responseCodeName});
        this.setState({color: this.props.adminMessage.responseColor});
        this.setState({roomName: this.props.adminMessage.roomName});
        this.setState({roomPassword: this.props.adminMessage.roomPassword});

        this.createAMessageTip(this.props.adminMessage);
        this.loadEmojis();

        //listeners server
        this.props.socket.on('onNewUserJoinedRoom', this.onNewUserJoinedRoom);
        this.props.socket.on('allUsersInTheRoom', this.onAllUsersInTheRoom);
        this.props.socket.on('sendMessageToTheRoom', this.onSendMessageToTheRoomBackEnd);
        this.props.socket.on('typing', this.onTyping);
        this.props.socket.on('leftRoom', this.onLeftRoom);

        //determines what happnes when an image gets selected change the color of the upload icon
        $("#file-upload").change(function(){
            if($("#file-upload")[0].files && $("#file-upload")[0].files[0]) {
                document.getElementById("upload-image-icon").setAttribute('style', 'color:#87CEEB !important');
            }
        });
    };

    //UTILITY FUNCTIONS
    //create a message tooltip
    createAMessageTip = (messageData) => {
        var imageUrl = "";
        if(messageData.imageData) {
            var arrayBufferView = new Uint8Array( messageData.imageData );
            var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
            var urlCreator = window.URL || window.webkitURL;
            imageUrl = urlCreator.createObjectURL( blob );
        }

        var classNameStyle = "";

        //test to see if this is the first message to derive my code name
        if(messageData.codeName === 'Admin') {
            classNameStyle = 'myMessage';
        } else {
            if(messageData.codeName == this.state.codeName) {
                classNameStyle = 'myMessage';
            } else {
                classNameStyle = 'someoneElsesMessage';
            }
        }

        //get a local val for the messages
        var localAllMessagesToolTip = this.state.allMessagesTooltip;

        //add to the list of messages
        localAllMessagesToolTip.push(
            <div id={this.state.allMessagesTooltip.length+'-single-message'} key={this.state.allMessagesTooltip.length} className={classNameStyle} style={{border: '2.5px solid '+messageData.color}} disabled={true}>
                <span className={'codeNameSpan'}>{messageData.codeName} Says</span>
                <div className={'actualMessageDiv'}>{messageData.message}</div>
                {imageUrl ? <img src={imageUrl} className={'imagePerPost'} onClick={() => this.openImageModalFn(imageUrl)}/> : ''}
                <span className={'timeSpan'}>{messageData.time}</span>
            </div>
        );

        //replace the older messages with the newly editted one
        this.setState({allMessagesTooltip : localAllMessagesToolTip});

        //scroll to the buttom of the list
        if(this.state.allMessagesTooltip.length > 2) {  
            $('#'+((this.state.allMessagesTooltip.length)-1)+'-single-message')[0].scrollIntoView({
                behavior: "smooth", // or "auto" or "instant"
                block: "start" // or "end"
            });
        }
    }

    //open Image Modal
    openImageModalFn = (imageUrl) => {
        this.setState({openImageDialog : true});
        this.setState({imageUrl : imageUrl});
    }

    closeImageModalFn = () => {
        this.setState({imageUrl : ""});
        this.setState({openImageDialog : false});
    }

    //this is to load all the emojis from the json file
    loadEmojis = () => {
        var allEmojisFormed = [];
        const result = _loadash.map(emojis, function(value, key) {
            allEmojisFormed.push({name:key, icon:value});
        });

        //build emoji div
        var tempAllEmojis = [];
        allEmojisFormed.forEach(emoji => {
            tempAllEmojis.push(<span key={emoji.name} id={emoji.name} className={'single-emoji'} onClick={this.useThisEmoji}>{emoji.icon}</span>);
        });

        this.setState({allEmojiIcons: tempAllEmojis});
    }

    //this is being called when a user clicks on the emoji
    useThisEmoji = (ev) => {
        $('#enter-message').val($('#enter-message').val() + ' '+ev.target.innerHTML+' ');
    }
    
    expandSideBar = () => {
        if(this.state.sideBarClosed){
            $("#side-bar").css({
                "z-index": "2"
            });
            this.setState({sideBarClosed: false});
        }else{
            $("#side-bar").css({
                "z-index": "0"
            });
            this.setState({sideBarClosed: true});
        }
    };

    openMenuBar = () => {
        if(this.state.menuBarClosed){
            $("#info-menu-list").css({
                "display": "block"
            });
            this.setState({menuBarClosed: false});
            
            $("#emoji-list").css({
                "display": "none"
            });
            this.setState({emojiBarClosed: true});
        }else{
            $("#info-menu-list").css({
                "display": "none"
            });
            this.setState({menuBarClosed: true});
        }
    };

    openEmojiMenu = () => {
        if(this.state.emojiBarClosed){
            $("#emoji-list").css({
                "display": "block"
            });
            this.setState({emojiBarClosed: false});
            
            $("#info-menu-list").css({
                "display": "none"
            });
            this.setState({menuBarClosed: true});
        }else{
            $("#emoji-list").css({
                "display": "none"
            });
            this.setState({emojiBarClosed: true});
        }
    }

    //LISTINING HANDLERS
    //when a new user joins tell other users that a new user just joined other than the one who joined
    onNewUserJoinedRoom = (messageData) => {
        messageData.message = messageData.message + " just Joined";
        this.createAMessageTip(messageData);
    };

    //this is for me to know everyone in the Room
    onAllUsersInTheRoom = (usersDetails) => {
        this.setState({allOthersIcon : []});

        var tempAllUsersIcon = [];
        usersDetails.forEach(user => {
            tempAllUsersIcon.push(
                <ListItem key={tempAllUsersIcon.length} className={'all-users-info-side-bar'} disabled={true} > 
                    <div className={'single-users-info-side-bar'}>
                        <i className="material-icons all-user-icon-list" style={{color: '#'+user.color }}>account_circle</i>
                        <div className={'all-user-code-name'}> {user.codeName.charAt(0).toUpperCase() + user.codeName.slice(1)} </div>
                    </div>
                </ListItem>
            );
        })
        
        this.setState({allOthersIcon : tempAllUsersIcon});
    }

    //when the user wants to send a message
    onSendMessageToTheRoomBackEnd = (messageData) => {
        this.createAMessageTip(messageData);
        $('#enter-message').val('');
    };

    //when the server sends down a typing message
    onTyping = (messageData) => {
        this.setState({whoIsTyping: messageData.codeName ? `${messageData.codeName } is Typing...` : ''});
    };
    
    //when the user is about to leave the room
    onLeftRoom = (messageData) => {
        this.createAMessageTip(messageData.user);
        this.onAllUsersInTheRoom(messageData.users);
    };

    //EVENT SENDING HANDLERS
    sendMessageToTheRoomFrontEnd = () => {
        if($('#enter-message').val() !== '') {
            var message = {
                codeName: this.state.codeName,
                message : $('#enter-message').val(), 
                color: this.state.color, 
                roomName: this.state.roomName,
                imageData : $("#file-upload")[0].files[0],
                socketId: undefined
            }
            this.state.socket.emit('sendMessageToTheRoom', message);

            //change upload icon color to white and remove the image
            document.getElementById("upload-image-icon").setAttribute('style', 'color:#ffffff !important');
            if($("#file-upload")[0].files && $("#file-upload")[0].files[0]) {
                $("#file-upload").replaceWith($("#file-upload").val('').clone(true));
            }
            
            //send to all users that this user is done and have no userName so that the engine can know that the user is done
            message = {
                codeName: '',
                roomName: this.state.roomName
            }
            this.props.socket.emit('typing', message);
        } else {
            $("#enter-message").css({
                "color": "red"
            });
        }
    };
    
    //listener for when send is pressed
    handlePressEnterSendMessage = (e) => {
        var code = (e.keyCode ? e.keyCode : e.which);

        var message = {
            codeName: this.state.codeName,
            roomName: this.state.roomName
        }
        this.props.socket.emit('typing', message);

        if (code == 13) { 
            this.sendMessageToTheRoomFrontEnd();
        } else {
            $("#enter-message").css({
                "color": "black"
            });
        }
    };

    //used to close all the pop ups
    closeAllPopUps = () => {
        $("#emoji-list").css({
            "display": "none"
        });
        this.setState({emojiBarClosed: true});
        
        $("#info-menu-list").css({
            "display": "none"
        });
        this.setState({menuBarClosed: true});
        
        $("#side-bar").css({
            "z-index": "0"
        });
        this.setState({sideBarClosed: true});
    }

    render () {
        return (
            <MuiThemeProvider>
                <div id={"main-container-chat-room"}>
                     {/* This is just to maintain consistency in from the main page because it needs it */}
                    <span id={"inner-container-whole"} style={{display: 'none' }}></span>
                    <div id={"side-bar"}>
                        <div id={"my-space"}>
                            <i id={"icons-person"} className="material-icons" style={{color: this.state.color }}>account_circle</i>
                        </div>

                        <div data-simplebar id={"others-space"}>
                            <List>
                                {this.state.allOthersIcon}
                            </List>
                        </div>

                        <div id={"show-all"}>
                            {this.state.sideBarClosed ?
                                <RightChevron id={"show-all-icon"} onClick={this.expandSideBar}/>
                                :
                                <LeftChevron id={"show-all-icon"} onClick={this.expandSideBar}/>
                            }
                        </div>
                    </div>

                    <div id={"whole-chat-space"}>
                        <div id={"top-bar"}>
                            <p id={"content-in-side-bar"}>{this.state.roomName}-Clan</p>
                        </div>
                        <div id={"messages-only"} onClick={this.closeAllPopUps}>
                            <div id={"message-only-list"}> {/* change back to list and sub items in maker to list item and add property */}
                                {this.state.allMessagesTooltip}
                            </div>
                        </div>
                        
                        <Dialog id={"dialog-for-image"} open={this.state.openImageDialog} onRequestClose={this.closeImageModalFn}>
                            <img id={"image-in-dialog"} src={this.state.imageUrl}/>
                        </Dialog>

                        <textarea id={"enter-message"} placeholder={"Enter Message Here"} onKeyPress={this.handlePressEnterSendMessage} onClick={this.closeAllPopUps}/>

                        <div id={"bottom-section-send"}>
                            <span id={'typing-message'}> {this.state.whoIsTyping} </span> 

                            <RaisedButton
                                id={"join-chat-button"}
                                className={"default-home-button"}
                                disabled={this.state.disableJoinButton}
                                onClick={this.sendMessageToTheRoomFrontEnd}
                            >Send</RaisedButton>
                            
                            {/* upload icon */}
                            <label htmlFor="file-upload">
                                <i id={"upload-image-icon"} className="material-icons" style={{color: this.state.color }}>cloud_upload</i>
                            </label>
                            <input type={'file'} id="file-upload" onChange={this.state.ChooseAnImage} accept="image/*"/>  {/* i might want to use the onclick, i used componentdidmount */}
                        
                            {/* emoji icon */}
                            <div id={'emoji-list'}>
                                <div id={'emoji-list-actual-content'} data-simplebar >
                                    {this.state.allEmojiIcons}
                                </div>
                            </div>
                            <i id={"toggle-emoji-display"} className="material-icons" style={{color: this.state.color }} onClick={this.openEmojiMenu}>sentiment_satisfied_alt</i>
                        
                            {/* menu icon */}
                            <div id={'info-menu-list'}>
                                <ul>
                                    <li> Room Name       : {this.state.roomName.charAt(0).toUpperCase() + this.state.roomName.slice(1)}</li>
                                    <li> Password        : {this.state.roomPassword}</li>
                                    <li> Your Chat Name  : {this.state.codeName}</li>
                                </ul>
                            </div>
                            <i id={"info-menu-list-icon"} className="material-icons" style={{color: this.state.color }} onClick={this.openMenuBar}>list</i>
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        )
    };
}


//
export default TalkHomePage;
