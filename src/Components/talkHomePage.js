//node defined importations
import React from 'react';
import io from 'socket.io-client';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';

//stylings importations
import '../Stylings/layout.css';
import '../Utility/layout.js';
import '../Utility/jscolor.js';

//personal elements
import ChatRoom from './chatRoom.js';

//definition of the main component starts here
class TalkHomePage extends React.Component {
    constructor () {
        super();

        //fields definition
        this.state = {
            socket : null,
            color : "605FFF",
            codeName : "",
            errorMessageCodeName : "",
            roomName : "",
            errorMessageRoomName : "",
            roomPassword : "",
            errorMessageRoomPassword : "",
            disableJoinButton : false, //change to true -- modify
            userCanJoinRoom : false,
            backEndError: "",
            adminMessage: null,
            gotDataForSnackBar: false,
            snackbarMsg: ''
        }
    };


    //
    componentDidMount = () => {
        // connect to server on Heroku clouds
        //connect to server
        //const socket = io.connect('localhost:5150', {'forceNew': true});
        //io.set('origins', '*:*');
        const socket = io.connect();
        //const socket = io.connect();
        this.setState({socket: socket});

        //listeners to mount
        socket.on('backEndError', this.onBackEndError);
        socket.on('userCanEnter', this.onUserCanEnter);
        socket.on('roomExists', this.onRoomExists);
        socket.on('welcome', this.onWelcome);
    };

    //definitions of functions
    //when the name is entered update the chatName field
    handleCodeNameChange = (e) => {
        this.setState({codeName: e.target.value});
        e.target.value === "" ? this.setState({errorMessageCodeName: " "}) : this.setState({errorMessageCodeName: ""});
        e.target.value !== "" && this.state.roomName !== "" && this.state.roomPassword !== "" ? this.setState({disableJoinButton: false}) : this.setState({disableJoinButton: true});
    };

    handleRoomNameChange = (e) => {
        this.setState({roomName: e.target.value});
        e.target.value === "" ? this.setState({errorMessageRoomName: " "}) : this.setState({errorMessageRoomName: ""});
        e.target.value !== "" && this.state.codeName !== "" && this.state.roomPassword !== "" ? this.setState({disableJoinButton: false}) : this.setState({disableJoinButton: true});
    };

    handleRoomPasswordChange = (e) => {
        this.setState({roomPassword: e.target.value});
        e.target.value === "" ? this.setState({errorMessageRoomPassword: " "}) : this.setState({errorMessageRoomPassword: ""});
        e.target.value !== "" && this.state.codeName !== "" && this.state.roomName !== "" ? this.setState({disableJoinButton: false}) : this.setState({disableJoinButton: true});
    };
    
    //handle snack bar close
    handleRequestClose = () => {
        this.setState({gotDataForSnackBar: false});
    }

    //defintions to Join a chat room
    joinChatRoom = (e) => {
        this.setState({color: document.getElementById('pick-a-color-input').value})
        var user = {color: document.getElementById('pick-a-color-input').value, codeName: this.state.codeName, roomName: this.state.roomName, password: this.state.roomPassword};  
        this.state.socket.emit('joinChatRoom', user);
    };

    //listener handlers
    onBackEndError = (errorData) => {
        this.setState({backEndError: errorData.errorMessage});
    }

    onUserCanEnter = (user) => {
        this.setState({backEndError: ""});
        this.setState({userCanJoinRoom: true});
    }

    onRoomExists = (roomExists) => {
        this.setState({snackbarMsg : roomExists ? "This Room Exists" : "You are Entereing a New Room"});
        this.setState({gotDataForSnackBar : true})
    }

    onWelcome = (adminMessage) => {
        this.setState({backEndError: ""});
        this.setState({adminMessage: adminMessage});
        this.setState({userCanJoinRoom: true});
    }

    //
    render () {
        return (
            <MuiThemeProvider>
                <div>
                    {this.state.userCanJoinRoom === false ?
                        <div id={"main-container"}>
                            <div id={"inner-container-whole"}>
                                <div id={"container-home-header"}>
                                    <div id={"home-logo"}></div>
                                    <span className={"massive-bold-header"}>Welcome To Clanchat</span>
                                    <span className={"tiny-small-header-description"}>A non-registration required group messaging App</span>
                                    {/* <RaisedButton
                                        id={"register-button"}
                                        className={"default-home-button"}>Register</RaisedButton> */}
                                </div>

                                <div id={"container-home-input-fields"}>
                                    <div>
                                        <span id={"pick-a-color-span"} className={"tiny-small-header-description"}> Pick a color below </span>
                                        <input
                                            id={"pick-a-color-input"}
                                            className={"jscolor"}
                                            placeholder={"Choose Your Color"}/>
                                    </div>
                                    <TextField
                                        id={"code-name"}
                                        className={"default-text-field"}
                                        placeholder={"Enter Chat Name"}
                                        onChange={this.handleCodeNameChange}
                                        value={this.state.codeName}
                                        errorText={this.state.errorMessageCodeName}/>
                                    <TextField
                                        id={"room-name"}
                                        className={"default-text-field"}
                                        placeholder={"Enter Existing or New Clan Name [Case Sensitive]"}
                                        onChange={this.handleRoomNameChange}
                                        value={this.state.roomName}
                                        errorText={this.state.errorMessageRoomName}/>
                                    <TextField
                                        id={"password"}
                                        className={"default-text-field"}
                                        placeholder={"Enter Room Password"}
                                        onChange={this.handleRoomPasswordChange}
                                        value={this.state.roomPassword}
                                        errorText={this.state.errorMessageRoomPassword}
                                        type={"password"}/>
                                    <div id={"status"}>{this.state.backEndError}</div>
                                    <RaisedButton
                                        id={"join-chat-button"}
                                        className={"default-home-button"}
                                        disabled={this.state.disableJoinButton}
                                        onClick={this.joinChatRoom}
                                        >Join Room</RaisedButton>
                                </div>

                                <span className={"tiny-small-header-description footer"}>&copy; www.samuelbalogun.com</span>
                            </div>
                        </div>
                        :
                        <div> <ChatRoom adminMessage={this.state.adminMessage} socket={this.state.socket}/></div>
                    }
                    <Snackbar
                        open={this.state.gotDataForSnackBar}
                        message={this.state.snackbarMsg}
                        autoHideDuration={2000}
                        onRequestClose={this.handleRequestClose}
                        id={'snack-bar'}
                    />
                </div>
            </MuiThemeProvider>
        )
    };
}


//
export default TalkHomePage;
