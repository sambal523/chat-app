import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TalkHomePage from './Components/talkHomePage';
//import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<TalkHomePage />, document.getElementById('root'));
//registerServiceWorker(); //the service worker helps in caching some of my computer data and storing some data so that when the network is slow for my client they
//can still have a good UI
