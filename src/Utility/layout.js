
var colors = ['240, 175, 142', '174, 120, 209', '255, 102, 102'];
var currentIndex = 0;
setInterval(function () {
    document.getElementById("inner-container-whole").style.backgroundColor = "rgba("+colors[currentIndex]+", 0.9)";
    if (!colors[currentIndex]) {
        currentIndex = 0;
    } else {
        currentIndex++;
    }
}, 500);