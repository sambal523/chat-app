var $ = require("jquery");


document.addEventListener('DOMContentLoaded', function() {
    //arrangeComponents();

    if(document.getElementById("side-bar")) {
        var colors = ['240, 175, 142', '174, 120, 209', '255, 102, 102'];
        var currentIndex = 0;
        setInterval(function () {
            document.getElementById("side-bar").style.backgroundColor = "rgba("+colors[currentIndex]+", 0.98)";
            if (!colors[currentIndex]) {
                currentIndex = 0;
            } else {
                currentIndex++;
            }
        }, 500);
    }
}, false);

$(window).resize(function(){
    //arrangeComponents();
});

// function wait(ms){
//     var start = new Date().getTime();
//     var end = start;
//     while(end < start + ms) {
//       end = new Date().getTime();
//    }
//  }

function arrangeComponents() {
    /*side bar*/
    console.log("in arrange components");
    console.log(($(document).height()));
    $("#side-bar").css({
        "height": ($(document).height())+"px"
    });
        /* others space */
    $("#others-space").css({
        "height": ($(document).height()-100)+"px !important"
    });

    /*main content*/
    $("#whole-chat-space").css({
        "width": ($(document).width()-50)+"px",
        "height": ($(document).height())+"px"
    });

    $("#bottom-section-send").css({
        "width": ($(document).width()-50)+"px"
    });

    $("#messages-only").css({
        "height": ($(document).height()-200)+"px"
    });

    $("#enter-message").css({
        "height": "150px"
    });
}


export default arrangeComponents;