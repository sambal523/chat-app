//this would act as the engine of the code
const moment = require('moment');
require('dotenv').config();
const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const port = process.env.PORT || 5150;

// Load the full build.
var _lodash = require('lodash');  /* findIndex: returns -1 if not found or the actual index of the element if found */

let app = express();
app.use(express.static('public'));

let server = http.createServer(app);
let io = socketIO(server);

//variable definitions
var color;
var users = [];
var rooms = [];

//class definitions
class Users {
    constructor(userObj) {
        this.codeName = userObj.codeName;
        this.color    = userObj.color;
        this.roomName = userObj.roomName;
        this.password = userObj.password;
    }
};

class ClientsResponse {
    constructor(messageData) {
        this.codeName = messageData.codeName;
        this.roomName = messageData.roomName;
        this.message  = messageData.message;
        this.color    = messageData.color;
        this.time     = messageData.time;
    }
};

checkIfUserExists = (user) => {
    return _lodash.findIndex(users, {codeName: user.codeName, roomName: user.roomName}) !=-1 ? true : false;
}

checkIfPasswordIsCorrect = (user) => {
    //check if the password is correct
    return _lodash.findIndex(rooms, {roomName: user.roomName, roomPassword: user.password}) !=-1 ? true : false;
}

checkIfRoomExists = (user) => {
    return _lodash.findIndex(users, {roomName: user.roomName}) !=-1 ? true : false;
}

getUserNamesAndPassword = (roomName) => {
     var allUserNamesAndPassword = [];
     users.forEach(user => {
        if(user.roomName === roomName) {    
            allUserNamesAndPassword.push({codeName : user.codeName, color: user.color});
        }
     });

     return allUserNamesAndPassword;
}

//actual code
io.on('connection', (socket) => {
    //defines what happens when a user requests to join a room
    socket.on('joinChatRoom', (user) => {
        if(!checkIfUserExists(user)) {
            //check if the passowrd is correct but the room has to exist first, else we dont need to check for the passowrd
            if(checkIfRoomExists(user) && !checkIfPasswordIsCorrect(user)) {
                io.to(socket.id).emit('backEndError', {errorMessage: 'Password is Invalid!'});
                return;
            }

            //check to see if that room exists and send it to the frontEnd
            if(!checkIfRoomExists(user)) {
                //add the room to the list of rooms and the password
                rooms.push({roomName: user.roomName, roomPassword: user.password});
                io.to(socket.id).emit('roomExists', false);
            } else {
                io.to(socket.id).emit('roomExists', true);
            }   

            //add the user to the list if not found
            user.socketId = socket.id;
            users.push(user);

            //add this room name to the list to join
            socket.join(user.roomName);

            //prepare Admin Message
            var adminMessage = {
                codeName : "Admin",
                roomName : user.roomName,
                responseCodeName : user.codeName,
                responseColor : '#'+user.color,
                roomPassword : user.password,
                message : "Welcome to "+user.roomName+" "+user.codeName,
                color: "#f0af8e",
                time:  moment().format('h:mm:ss a')
            };

            //send the user the welcome message
            io.to(socket.id).emit('welcome', adminMessage);

            //Admin: tell everyone in the room about the new user   
            adminMessage.message = user.codeName; // will append 'just Joined' to tell that a new user just joined
            socket.broadcast.to(user.roomName).emit('onNewUserJoinedRoom', adminMessage);

            //tell all the users and the new one inclusive of all the users in the Room
            io.sockets.in(user.roomName).emit('allUsersInTheRoom', getUserNamesAndPassword(user.roomName));
        }else{
            //send a message to the front end saying that the current user already exists        
            io.to(socket.id).emit('backEndError', {errorMessage: 'ChatName is Taken, Choose Another!'});
        }
    });

    //when a user wants to send a messgage to a Room  
    socket.on('sendMessageToTheRoom', (user) => {
        var message = {
            codeName : user.codeName,
            roomName : user.roomName,
            message : user.message,
            color: user.color,
            time:  moment().format('h:mm:ss a'),
            imageData: user.imageData
        };
        io.sockets.in(user.roomName).emit('sendMessageToTheRoom', message);
    });

    //when a user is typing
    socket.on('typing', (user) => {
        socket.broadcast.to(user.roomName).emit('typing', {codeName: user.codeName});
    });

    //determines what would happen when a client disconnects
    socket.on('disconnect', () => {
        //search for the user that is about to be deleted
        var user = _lodash.find(users, {socketId: socket.id});

        if(user) {
            _lodash.remove(users, {socketId: socket.id});

            //prepare Admin Message
            var adminMessage = {
                codeName : "Admin",
                roomName : user.roomName,
                responseCodeName : user.codeName,
                responseColor : '#'+user.color,
                message : user.codeName+" just left!!! 😢",
                color: "#f0af8e",
                time:  moment().format('h:mm:ss a')
            };
        
            //tell the users in that room that a fellow just left
            socket.broadcast.to(user.roomName).emit('leftRoom', {user : adminMessage, users : users});
        }
    });
});

//home page
app.get('/', function(req, res) {
    res.sendFile('index.html', {root :__dirname  +'/public'});
})

server.listen(port, () => {
 console.log(`starting on port ${port}`);
})
